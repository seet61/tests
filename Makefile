#!/usr/bin/env make -rRf

PROJECT_NAME := yourwave_platform
SUB_PROJECTS := common teleport metrics messaging db yweb yws dirble proto
GIT_BASE_URI := "git@bitbucket.org:yourwave/"

ifndef VERBOSE
MAKEFLAGS += --no-print-directory
endif

.PHONY: all
all: run
	@echo "--- All done. -------------------------------------------"

run: get-deps
	python test.py 

get-deps: 
	@echo "--- Checking selenium for python... ---------------------"
	
	@ if [$e -z]; then pip instal selenium; fi;
	@echo "--- Checking selenium for python end --------------------"
