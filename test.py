#!/home/seet/yourwave/test/example
#-*- coding: utf-8 -*-

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import strftime

day = strftime('%Y%m%d%H%M') 

class YandexSerch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def test_search_in_yandex(self):
        driver = self.driver
        driver.get("http://yandex.ru")
        self.assertIn(u'Яндекс', driver.title)
        elem = driver.find_element_by_xpath("//input[@class='input__control input__input']")
        elem.send_keys("google")
        elem = driver.find_element_by_xpath("//button[@type='submit']")
        elem.click()
        driver.save_screenshot('./images/screen.png')
        '''assert "No results found." not in driver.page_source'''

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
        unittest.main()
        